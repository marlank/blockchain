// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

contract CrownFunding {
    enum FundraisingState {
        Opened,
        Closed
    }


    struct Contribution {
        address contributor;
        uint256 value;
    }

      struct Project {
        string id;
        string name;
        string description;
        address payable author;
        FundraisingState state;
        uint funds;
        uint fundraisingGoal;
    }

    Project[] public projects;
    mapping(string => Contribution[]) public contributions;

    
      
    event ProjectCreated(string projectID, string name, string description, uint FundraisingState);
    event projectFunded(string projectId, uint value);
    event projectStateChange(string id, FundraisingState state);
    error stateClose(FundraisingState state);

    modifier ifAuthor(uint projectIndex) {
        require(projects[projectIndex].author == msg.sender, "You need to be the project author");
        //la función es insertada en donde aparece este símbolo
        _;
    }


    modifier ifNotAuthor(uint projectIndex) {
        require(projects[projectIndex].author != msg.sender, "As author you can not fund your own project");
        //la función es insertada en donde aparece este símbolo
        _;
    }



    function createProject(string calldata id, string calldata name, string calldata description, uint fundraisingGoal) public{
        require(fundraisingGoal > 0, "Fundraising goals must be greater than 0");
        Project memory project = Project(id, name, description, payable(msg.sender), FundraisingState.Opened, 0, fundraisingGoal);
        projects.push(project);
        emit ProjectCreated(id, name, description, fundraisingGoal);
    }

    function fundProject(uint projectIndex) public payable ifNotAuthor(projectIndex) {
        Project memory project = projects[projectIndex];
        require(project.state != FundraisingState.Closed, 'This project is closed,you cannot add more funds');
        require(msg.value > 0, 'The contribution must be higher 1 Eth');
        project.author.transfer(msg.value);
        project.funds += msg.value;
        projects[projectIndex] = project;

        contributions[project.id].push(Contribution(msg.sender, msg.value));

        emit projectFunded(project.id, msg.value);
    }  

    function changeProjectState(FundraisingState newState, uint projectIndex) public ifAuthor(projectIndex) {
        Project memory project = projects[projectIndex];
        require(project.state != newState, 'New state must be different');    
        project.state = newState;
        projects[projectIndex] = project;
        emit projectStateChange(project.id, newState);
    }
}
